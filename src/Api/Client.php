<?php

namespace Northq\API\PHP\Api;

use Northq\API\PHP\Common\RestErrorCode;

define('BACKEND_BASE_URI',         'https://dev.homemanager.tv/');
define('BACKEND_SERVICES_URI',     'https://dev.homemanager.tv/main');
define('BACKEND_ACCESS_TOKEN_URI', 'https://dev.homemanager.tv/token/new.json');
define('BACKEND_AUTHORIZE_URI',    'https://dev.homemanager.tv/oauth2/authorize');

/**
 * OAuth2.0 Northq client-side implementation.
 */
class Client
{
    /**
     * Array of persistent variables stored.
     */
    protected $conf = array();
    protected $token;
    protected $user;
    protected $version;

    /**
     * Default options for cURL.
     */
    public static $CURL_OPTS = array(
        CURLOPT_CONNECTTIMEOUT => 10,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_HEADER         => TRUE,
        CURLOPT_TIMEOUT        => 60,
        CURLOPT_USERAGENT      => 'northqclient',
        CURLOPT_SSL_VERIFYPEER => TRUE,
        CURLOPT_HTTPHEADER     => array('Accept: application/json'),
    );

    /**
     * Initialize a NA OAuth2.0 Client.
     *
     * @param $config
     *   An associative array as below:
     *   - code: (optional) The authorization code.
     *   - username: (optional) The username.
     *   - password: (optional) The password.
     *   - client_id: (optional) The application ID.
     *   - client_secret: (optional) The application secret.
     *   - access_token: (optional) A stored access_token to use
     *   - object_cb : (optionale) An object for which func_cb method will be applied if object_cb exists
     *   - func_cb : (optional) A method called back to store tokens in its context (session for instance)
     */
    public function __construct($config = array())
    {
        if (isset($config['token'])) {
            $this->token = $config['token'];
        }
        if (isset($config['user'])) {
            $this->user = $config['user'];
        }

        $uri = array(
            'base_uri'         => BACKEND_BASE_URI,
            'services_uri'     => BACKEND_SERVICES_URI,
            'access_token_uri' => BACKEND_ACCESS_TOKEN_URI,
            'authorize_uri'    => BACKEND_AUTHORIZE_URI,
        );
        foreach ($uri as $key => $val) {
            if (isset($config[$key])) {
                $this->setVariable($key, $config[$key]);
                unset($config[$key]);
            } else {
                $this->setVariable($key, $val);
            }
        }

        foreach ($config as $name => $value) {
            $this->setVariable($name, $value);
        }
    }

    /**
     * Sets a persistent variable.
     *
     * To avoid problems, always use lower case for persistent variable names.
     *
     * @param $name
     *   The name of the variable to set.
     * @param $value
     *   The value to set.
     */
    public function setVariable($name, $value)
    {
        $this->conf[$name] = $value;
        return $this;
    }

    /**
     * Returns a persistent variable.
     *
     * To avoid problems, always use lower case for persistent variable names.
     *
     * @param $name
     *   The name of the variable to return.
     * @param $default
     *   The default value to use if this variable has never been set.
     *
     * @return
     *   The value of the variable.
     */
    public function getVariable($name, $default = NULL)
    {
        return isset($this->conf[$name]) ? $this->conf[$name] : $default;
    }

    private function setTokens($value)
    {
        if (isset($value['token'])) {
            $this->token = $value['token'];
        }
        if (isset($value['user'])) {
            $this->user = $value['user'];
        }
    }

    public function unsetTokens()
    {
        $this->token = null;
    }

    /**
     * Makes an HTTP request
     *
     * This method can be overriden by subclasses if developers want to do
     * fancier things or use something other than cURL to make the request.
     *
     * @param $path   The target path, relative to base_path/service_uri or an absolute URI.
     * @param $method (optional) The HTTP method (default 'GET').
     * @param $params (optional The GET/POST parameters.
     * @param $ch     (optional) An initialized curl handle
     *
     * @return The json_decoded result or ClientException if pb happend
     */
    public function makeRequest($path, $method = 'GET', $params = array())
    {
        $ch   = curl_init();
        $opts = self::$CURL_OPTS;
        if ($params) {
            switch ($method) {
                case 'GET':
                    $path .= '?' . http_build_query($params, NULL, '&');
                    break;
                // Method override as we always do a POST.
                default:
                    if ($this->getVariable('file_upload_support')) {
                        $opts[CURLOPT_POSTFIELDS] = $params;
                    } else {
                        $opts[CURLOPT_POSTFIELDS] = http_build_query($params, NULL, '&');
                    }
                    break;
            }
        }
        $opts[CURLOPT_URL] = $path;
        // Disable the 'Expect: 100-continue' behaviour. This causes CURL to wait
        // for 2 seconds if the server does not support this header.
        if (isset($opts[CURLOPT_HTTPHEADER])) {
            $existing_headers   = $opts[CURLOPT_HTTPHEADER];
            $existing_headers[] = 'Expect:';
            $ip                 = $this->getVariable('ip');
            if ($ip) {
                $existing_headers[] = 'CLIENT_IP: '.$ip;
            }
            $opts[CURLOPT_HTTPHEADER] = $existing_headers;
        } else {
            $opts[CURLOPT_HTTPHEADER] = array('Expect:');
        }
        curl_setopt_array($ch, $opts);
        $result = curl_exec($ch);

        $errno = curl_errno($ch);
        // CURLE_SSL_CACERT || CURLE_SSL_CACERT_BADFILE
        if ($errno == 60 || $errno == 77) {
            echo "WARNING ! SSL_VERIFICATION has been disabled since ssl error retrieved. ".
                "Please check your certificate http://curl.haxx.se/docs/sslcerts.html\n";
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            $result = curl_exec($ch);
        }

        if ($result === FALSE) {
            $e = new Exception\CurlErrorTypeException(curl_errno($ch), curl_error($ch));
            curl_close($ch);
            throw $e;
        }
        curl_close($ch);

        // Split the HTTP response into header and body.
        list($headers, $body) = explode("\r\n\r\n", $result);

        $headers = explode("\r\n", $headers);
        //Only 2XX response are considered as a success
        if (strpos($headers[0], 'HTTP/1.1 2') !== FALSE) {
            $decode = json_decode($body, TRUE);
            if (!$decode) {
                if (preg_match('/^HTTP\/1.1 ([0-9]{3,3}) (.*)$/', $headers[0], $matches)) {
                    throw new Exception\JsonErrorTypeException($matches[1], $matches[2]);
                } else {
                    throw new Exception\JsonErrorTypeException(200, 'OK');
                }
            }
            return $decode;
        } else {
            if (!preg_match('/^HTTP\/1.1 ([0-9]{3,3}) (.*)$/', $headers[0], $matches)) {
                $matches = array('', 400, 'bad request');
            }
            $decode = json_decode($body, TRUE);
            if (!$decode) {
                throw new Exception\ApiErrorTypeException($matches[1], $matches[2], null);
            }
            throw new Exception\ApiErrorTypeException($matches[1], $matches[2], $decode);
        }
    }

    /**
     * Retrieve an access token following the best grant to recover it (order id : code, password)
     *
     * @return array A valid array containing at least an access_token as an index
     *
     * @throws InternalErrorTypeException If unable to retrieve an access_token
     */
    public function getAccessToken()
    {
        if ($this->token) {
            return array(
                'token' => $this->token,
                'user'  => $this->user,
            );
        }
        if ($this->getVariable('username') && $this->getVariable('password')) {
            return $this->getAccessTokenFromPassword($this->getVariable('username'), $this->getVariable('password'));
        } else {
            throw new Exception\InternalErrorTypeException('No access token stored');
        }
    }

    /**
     * Get access token from OAuth2.0 token endpoint with basic user
     * credentials.
     *
     * This function will only be activated if both username and password
     * are setup correctly.
     *
     * @param $username Username to be check with.
     * @param $password Password to be check with.
     * @return array A valid OAuth2.0 JSON decoded access token in associative array
     * @throws InternalErrorTypeException If unable to retrieve an access_token
     */
    private function getAccessTokenFromPassword($username, $password)
    {
        if ($this->getVariable('access_token_uri')) {
            $response = $this->makeRequest(
                $this->getVariable('access_token_uri'),
                'POST',
                array(
                    'username' => $username,
                    'password' => $password,
                )
            );
            $this->setTokens($response);
            return $response;
        } else {
            throw new Exception\InternalErrorTypeException('missing args for getting password grant');
        }
    }

    /**
     * Make an OAuth2.0 Request.
     *
     * Automatically append "access_token" in query parameters
     *
     * @param $path
     *   The target path, relative to base_path/service_uri
     * @param $method
     *   (optional) The HTTP method (default 'GET').
     * @param $params
     *   (optional The GET/POST parameters.
     *
     * @return
     *   The JSON decoded response object.
     *
     * @throws OAuth2Exception
     */
    protected function makeOAuth2Request($path, $method = 'GET', $params = array(), $reget_token = true)
    {
        try {
            $res = $this->getAccessToken();
        } catch(Exception\ApiErrorTypeException $ex) {
            throw new Exception\NotLoggedErrorTypeException($ex->getCode(), $ex->getMessage());
        }
        $params['token'] = $res['token'];
        $params['user']  = $res['user'];
        try {
            $res = $this->makeRequest($path, $method, $params);
            return $res;
        } catch(Exception\ApiErrorTypeException $ex) {
            if ($reget_token == true) {
                switch ($ex->getCode()) {
                    case RestErrorCode::INVALID_ACCESS_TOKEN:
                    case RestErrorCode::ACCESS_TOKEN_EXPIRED:
                        try {
                            $this->getAccessTokenFromRefreshToken();//exception will be thrown otherwise
                        } catch(\Exception $ex2) {
                            //Invalid refresh token TODO: Throw a special exception
                            throw $ex;
                        }

                        return $this->makeOAuth2Request($path, $method, $params, false);
                    break;
                    default:
                        throw $ex;
                }
            } else {
                throw $ex;
            }
        }
        return $res;
    }

    /**
     * Make an API call.
     *
     * Support both OAuth2.0 or normal GET/POST API call, with relative
     * or absolute URI.
     *
     * If no valid OAuth2.0 access token found in session object, this function
     * will automatically switch as normal remote API call without "access_token"
     * parameter.
     *
     * Assume server reply in JSON object and always decode during return. If
     * you hope to issue a raw query, please use makeRequest().
     *
     * @param $path
     *   The target path, relative to base_path/service_uri or an absolute URI.
     * @param $method
     *   (optional) The HTTP method (default 'GET').
     * @param $params
     *   (optional The GET/POST parameters.
     *
     * @return
     *   The JSON decoded body response object.
     *
     * @throws ClientException
     */
    public function api($path, $method = 'GET', $params = array(), $secure = false)
    {
        if (is_array($method) && empty($params)) {
            $params = $method;
            $method = 'GET';
        }

        // json_encode all params values that are not strings.
        foreach ($params as $key => $value) {
            if (!is_string($value)) {
                $params[$key] = json_encode($value);
            }
        }

        $res = $this->makeOAuth2Request($this->getUri($path, array(), $secure), $method, $params);
        if (isset($res['body'])) {
            return $res['body'];
        } else {
            return $res;
        }
    }

    /**
     * Since $_SERVER['REQUEST_URI'] is only available on Apache, we
     * generate an equivalent using other environment variables.
     */
    function getRequestUri()
    {
        if (isset($_SERVER['REQUEST_URI'])) {
            $uri = $_SERVER['REQUEST_URI'];
        } else {
            if (isset($_SERVER['argv'])) {
                $uri = $_SERVER['SCRIPT_NAME'] . '?' . $_SERVER['argv'][0];
            } elseif (isset($_SERVER['QUERY_STRING'])) {
                $uri = $_SERVER['SCRIPT_NAME'] . '?' . $_SERVER['QUERY_STRING'];
            } else {
                $uri = $_SERVER['SCRIPT_NAME'];
            }
        }
        // Prevent multiple slashes to avoid cross site requests via the Form API.
        $uri = '/' . ltrim($uri, '/');

        return $uri;
    }

    /**
     * Returns the Current URL.
     *
     * @return
     *   The current URL.
     */
    protected function getCurrentUri()
    {
        $protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on'
            ? 'https://'
            : 'http://';
        $current_uri = $protocol . $_SERVER['HTTP_HOST'] . $this->getRequestUri();
        $parts = parse_url($current_uri);

        $query = '';
        if (!empty($parts['query'])) {
            $params = array();
            parse_str($parts['query'], $params);
            $params = array_filter($params);
            if (!empty($params)) {
                $query = '?' . http_build_query($params, NULL, '&');
            }
        }

        // Use port if non default.
        $port = isset($parts['port'])
            && (
                ($protocol === 'http://' && $parts['port'] !== 80)
                || ($protocol === 'https://' && $parts['port'] !== 443)
            )
            ? ':' . $parts['port'] : '';

        // Rebuild.
        return $protocol . $parts['host'] . $port . $parts['path'] . $query;
    }

    /**
     * Build the URL for given path and parameters.
     *
     * @param $path
     *   (optional) The path.
     * @param $params
     *   (optional) The query parameters in associative array.
     *
     * @return
     *   The URL for the given parameters.
     */
    protected function getUri($path = '', $params = array(), $secure = false)
    {
        $url = $this->getVariable('services_uri') ? $this->getVariable('services_uri') : $this->getVariable('base_uri');
        if ($secure == true) {
            $url = self::strReplaceOnce('http', 'https', $url);
        }
        if (!empty($path)) {
            if (substr($path, 0, 4) == 'http') {
                $url = $path;
            } elseif(substr($path, 0, 5) == 'https') {
                $url = $path;
            } else {
                $url = rtrim($url, '/') . '/' . ltrim($path, '/');
            }
        }

        if (!empty($params)) {
            $url .= '?' . http_build_query($params, NULL, '&');
        }

        return $url;
    }

    public static function strReplaceOnce($str_pattern, $str_replacement, $string)
    {
        if (strpos($string, $str_pattern) !== false) {
            $occurrence = strpos($string, $str_pattern);
            return substr_replace($string, $str_replacement, strpos($string, $str_pattern), strlen($str_pattern));
        }
        return $string;
    }
}
