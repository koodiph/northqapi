<?php

namespace Northq\API\PHP\Api;

class Helper
{
    public $client;

    public function __construct($client)
    {
        $this->client = $client;
    }

    public function api($method, $action, $params = array())
    {
        if (isset($this->client)) {
            return $this->client->api($method, $action, $params);
        } else {
            return NULL;
        }
    }
}
