<?php

include __DIR__ . '/../../vendor/autoload.php';
include __DIR__ . '/../config.php';

use Northq\API\PHP\Api\Client;
use Northq\API\PHP\Api\Helper;
use Northq\API\PHP\Api\Exception\ClientException;

$token = '3uf-e2fd1a8f95ed5e2ba163';
$user  = 136;

$client = new Client(array(
    'username' => $test_username,
    'password' => $test_password,
    'user'     => $user,
));

try {
    $path     = 'getCurrentUserHouses';
    $method   = 'GET';
    $params   = array();
    $response = $client->api($path, $method, $params);
    echo '<br/><br/>Current user houses: <br/>';
    var_dump($response);
} catch (\Exception $e) {
    throw $e;
}

try {
    $path     = 'getUserCameras';
    $method   = 'GET';
    $params   = array();
    $response = $client->api($path, $method, $params);
    echo '<br/><br/><br/>User cameras: <br/>';
    var_dump($response);
} catch (\Exception $e) {
    throw $e;
}
