<?php

include __DIR__ . '/../../vendor/autoload.php';
include __DIR__ . '/../config.php';

use Northq\API\PHP\Api\Client;
use Northq\API\PHP\Api\Helper;
use Northq\API\PHP\Api\Exception\ClientException;

$client = new Client(array(
    'username' => $test_username,
    'password' => $test_password,
    'user'     => $user,
));

$tokens = $client->getAccessToken();
var_dump($tokens);
